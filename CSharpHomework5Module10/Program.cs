﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;
using CSharpHomework5Module10.House;
using CSharpHomework5Module10.Team;

namespace CSharpHomework5Module10
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TeamLeader leader = new TeamLeader();
                TeamBase dreamTeam = new TeamBase(5);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(0);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(1);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(2);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(3);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(4);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(0);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(4);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(1);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(2);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(3);
                leader.BuildStatus();
                Console.WriteLine();

                dreamTeam.WorkerOnJob(4);
                leader.BuildStatus();
                Console.WriteLine();
            }
            catch (IndexOutOfRangeException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (FormatException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
