﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.House;

namespace CSharpHomework5Module10.Interfaces
{
    public interface IWorker
    {
        void BuildStatus();
    }
}
