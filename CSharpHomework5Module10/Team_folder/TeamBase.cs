﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;
using CSharpHomework5Module10.House;

namespace CSharpHomework5Module10.Team
{
    public class TeamBase
    {
        private Worker[] worker;
        public int Size { get; private set; }

        public TeamBase(int size)
        {
            Size = size;
            worker = new Worker[5];
            for (int i = 0; i < Size; i++)
            {
                worker[i] = new Worker();
            }
        }

        public void WorkerOnJob(int index)
        {
            worker[index].BuildStatus();
        }

    }
}
