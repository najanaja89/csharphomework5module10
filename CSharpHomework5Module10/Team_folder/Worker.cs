﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;
using CSharpHomework5Module10.House;

namespace CSharpHomework5Module10.Team
{

    public class Worker : IWorker
    {
        public string Name { get; private set; }
        public Worker()
        {
            Name = "Worker";
        }
        public void BuildStatus()
        {

            if (HouseBase.BasementCount < 1)
            {
                HouseBase.BasementCount++;
                return;
            }

            if (HouseBase.DoorCount < 1)
            {
                HouseBase.DoorCount++;
                return;
            }
            if (HouseBase.RoofCount < 1)
            {
                HouseBase.RoofCount++;
                return;
            }

            if (HouseBase.WallCount < 4)
            {
                HouseBase.WallCount++;
                return;
            }

            if (HouseBase.WindowCount < 4)
            {
                HouseBase.WindowCount++;
                return;
            }
        }
    }
}
