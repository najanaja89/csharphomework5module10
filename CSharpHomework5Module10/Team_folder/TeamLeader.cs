﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;
using CSharpHomework5Module10.House;

namespace CSharpHomework5Module10.Team
{
    public class TeamLeader : IWorker
    {
        public void BuildStatus()
        {

            IPart basement = new Basement();
            basement.msg();

            IPart door = new Door();
            door.msg();

            IPart roof = new Roof();
            roof.msg();

            IPart wall = new Wall();
            wall.msg();

            IPart windows = new Window();
            windows.msg();
        }
    }
}
