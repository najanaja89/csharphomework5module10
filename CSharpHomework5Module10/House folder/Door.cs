﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;

namespace CSharpHomework5Module10.House
{
    public class Door : HouseBase, IPart
    {
        public string Build { get; private set; }

        public Door()
        {
            Build = "Door";
        }
        public void msg()
        {
            if (HouseBase.DoorCount < 1)
            {
                Console.WriteLine(Build + " in progress");
            }
            else Console.WriteLine(Build + " is ready");
        }
    }
}
