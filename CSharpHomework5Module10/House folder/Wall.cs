﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;

namespace CSharpHomework5Module10.House
{
    public class Wall : HouseBase, IPart
    {
        public string Build { get; private set; }

        public Wall()
        {
            Build = "Wall";
        }

        public void msg()
        {
            if (HouseBase.WallCount < 4)
            {
                Console.WriteLine(Build + " in progress");
            }
            else Console.WriteLine(Build + " is ready");
        }
    }
}
