﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;

namespace CSharpHomework5Module10.House
{
    public class Basement : HouseBase, IPart
    {
        public string Build { get; private set; }

        public Basement()
        {
            Build = "Basement";
        }
        public void msg()
        {
            if (HouseBase.BasementCount < 1)
            {
                Console.WriteLine(Build + " in progress");
            }
            else Console.WriteLine(Build + " is ready");

        }
    }
}
