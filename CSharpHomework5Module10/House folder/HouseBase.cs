﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;

namespace CSharpHomework5Module10.House
{
    public class HouseBase
    {
        public static int BasementCount { get; set; }
        public static int WallCount { get; set; }
        public static int DoorCount { get; set; }
        public static int RoofCount { get; set; }
        public static int WindowCount { get; set; }


        static HouseBase()
        {
            BasementCount = 0;
            WallCount = 0;
            DoorCount = 0;
            RoofCount = 0;
            WindowCount = 0;
        }

    }
}
