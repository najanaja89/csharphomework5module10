﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpHomework5Module10.Interfaces;

namespace CSharpHomework5Module10.House
{
    public class Window : HouseBase, IPart
    {
        public string Build { get; private set; }

        public Window()
        {
            Build = "Window";
        }

        public void msg()
        {
            if (HouseBase.WindowCount < 4)
            {
                Console.WriteLine(Build + " in progress");
            }
            else Console.WriteLine(Build + " is ready");
        }
    }
}
